package com.example.testingdemos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import network.ApiClient

class MainActivity : AppCompatActivity() {


    var listOfItem:MutableList<Int>?= mutableListOf()
    var listDd:MutableList<Int>?= mutableListOf()
    private var recyclerView:RecyclerView?=null
    private var mLayoutManager:LinearLayoutManager?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Log.d("onCreate","Yes")
        setContentView(R.layout.activity_main)
        mLayoutManager= LinearLayoutManager(this)
        recyclerView?.layoutManager=mLayoutManager

    }

    override fun onStart() {
        super.onStart()
        Log.d("Start", "Yes")
    }

    override fun onResume() {
        super.onResume()
        Log.d("onResume", "Yes")

        try
        {
        var i=10/0
        }
        catch (e:Exception)
        {
            Log.d("Exception", "Yes")
        }
        finally {
            Log.d("finally", "Yes")
        }
        for (i in 0 until 100)
        {
            listOfItem?.add(i, i)
        }

        Toast.makeText(this,"Testing",Toast.LENGTH_LONG).show()
        Log.d("onResume", "main" + Thread.currentThread().name)

        runBlocking {


            Log.d("onResume", "resp" + Thread.currentThread().name)
            Log.d("onResume", "listOfItem" + listOfItem?.size)



            var job = lifecycleScope.launch(Dispatchers.Default) {
                Log.d("onResumeResponse", "resp" + Thread.currentThread().name)
                //  var resp = ApiClient.apiInterface.getLoginData()
                listOfItem?.forEach {
                    if (it % 6 == 0) {
                        Log.d("Divide", "${it}")
                        delay(20)
                    }
                }
                var listFiltered = listOfItem?.find { it % 2 == 0 }
                Log.d("listFiltered", "$listFiltered")
                var localList = async {
                    getList()
                }
                Log.d("list", "${localList.await()}")

            }
  job.join()
          //job.cancelAndJoin()
        }

    }

    override fun onPause() {
        super.onPause()
        Log.d("onPause","Yes")
    }

    override fun onStop() {
        super.onStop()
        Log.d("onStop","Yes")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("onRestart","Yes")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("onDestroy","Yes")
    }


    private  fun getList():MutableList<Int>{

        return listOfItem!!
    }
}