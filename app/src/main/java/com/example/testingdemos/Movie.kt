package com.example.testingdemos

data class Movie(var title: String, var image: String)