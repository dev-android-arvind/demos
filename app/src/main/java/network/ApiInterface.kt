package network

import com.example.testingdemos.Movie
import com.example.testingdemos.QuoteList
import retrofit2.http.GET

interface ApiInterface {
    @GET("/quotes")
    suspend fun getLoginData(): QuoteList

    @GET
    suspend fun getMovieList(): List<Movie>
}