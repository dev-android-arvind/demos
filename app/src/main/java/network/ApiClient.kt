package network

import android.util.JsonReader
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


var baseURL = "https://quotable.io/"
//var baseURL = "http://velmm.com/apis/"


object ApiClient {


   // var gsonBuilder=GsonBuilder().setLenient().create()


    private fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build()
    }

    val apiInterface: ApiInterface
        get() {
            return getInstance().create(ApiInterface::class.java)
        }

}